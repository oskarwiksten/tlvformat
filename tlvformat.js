function looksLikeJson(text) {
	return /^\{.*.\}$/.test(text);
}

function formatAsJson(div) {
	var text = div.text();
	if (!looksLikeJson(text)) {
		return;
	}
	var obj = JSON.parse(text);
	var formatted = JSON.stringify(obj, null, 2);
	var pre = $("<pre>").addClass("json").text(formatted);
	div.empty();
	div.append(pre);

	placeLinksOnEmvProtocolParts(pre, obj);
}

function looksLikeiZettleBase64(text) {
	return /^[A-Za-z0-9\-_+/\s]+={0,2}$/.test(text);
}

function parseHexToBinaryArray(hex) {
	hex = hex.replace(/[^A-Fa-f0-9]/g, '')
    var L = 0;
    var result = new Array(hex.length / 2);
    for (var i = 0; i < hex.length; i += 2) {
        result[L++] = parseInt(hex.substr(i, 2), 16);
    }
    return result;
}

function parseiZettleBase64ToBinaryArray(v) {
	v = v.replace(/[^A-Za-z0-9\-+_/=]/g, '')
    var padding = (4 - (v.length % 4)) % 4;
    // Fix iZettle-isms of b64 encodings (uses different charset, sometimes without '=' padding)
    v = v.replace('-', '+').replace('_', '/') + '='.repeat(padding);
    return base64js.toByteArray(v);
}

function describeEmvProtocolPacket(packet, div, parsingType) {
    var packetAsBinary = '';
	if (!parsingType) {
		if (looksLikeHex(packet)) {
			parsingType = 'hex';
		} else if (looksLikeJavaStringOfHex(packet)) {
			parsingType = 'hex';
		} else if (looksLikeiZettleBase64(packet)) {
			parsingType = 'base64';
		}
	}
	
    if ('hex' == parsingType) {
        packetAsBinary = parseHexToBinaryArray(packet);
    } else if ('base64' == parsingType) {
        packetAsBinary = parseiZettleBase64ToBinaryArray(packet);
    }

    if (packetAsBinary) {
        div.append($("<pre>").text(describeBinaryEmvProtocolPacket(packetAsBinary, 0)));
    } else {
        div.text(packet);
    }
}

function toHex(d) {
    return ("0"+(Number(d).toString(16))).slice(-2).toUpperCase()
}

function arrayToHex(d) {
    var hex = '';
    for (var i = 0; i < d.length; ++i) {
        hex += toHex(d[i]);
    }
    return hex;
}

function parseTLV(data, startIdx) {
    var result = {};
    var i = startIdx;
    var lastIdx = (data.length - 1);
    if (i >= lastIdx) { return false; }
    var b = data[i++];
    result.tag = toHex(b);
    if ((b & 0x1f) == 0x1f) {
        do {
            if (i == lastIdx) { return false; }
            result.tag += toHex(b = data[i++]);
        } while ((b & 0x80) == 0x80);
    }
    if (result.tag.startsWith('0')) { return false; }
    b = data[i++];
    var len = 0;
    result.length = toHex(b);
    if ((b & 0x80) == 0x80) {
        var n = b & 0x7F;
        if (n == 0) { return false; }
        if (n + i >= lastIdx) { return false; }
        var length = arrayToHex(data.slice(i, i + n));
        i += n;
        result.length += length;
        len = parseInt(length, 16);
    } else {
        len = b;
    }
    result.nextIdx = i + len;
    if (result.nextIdx > data.length) { return false; }
    result.value = data.slice(i, result.nextIdx);
    return result;
}

function allBytesAreSame(data) {
	if (!data) { return false; }
	if (!data.length) { return false; }
	var b = data[0];
	for(var i = 1; i < data.length; ++i) {
		if (data[i] != b) { return false; }
	}
	return true;
}

function parseTLVArray(data, startIdx) {
    var i = startIdx;
    var result = {tlvs: [], rest: ''};
    while(i < data.length) {
        var tlv = parseTLV(data, i);
        if (!tlv) {
			var rest = data.slice(i);
			if (allBytesAreSame(rest)) {
				result.rest = arrayToHex(rest);
				return result;
			}
			return false;
		}
        result.tlvs.push(tlv);
        i = tlv.nextIdx;
        if (i == data.length) {
            return result;
        }
    }
    return false;
}

function describeBinaryEmvProtocolPacket(packet) {
    if (!packet || packet.length == 0) { return ''; }
    var minHeaderLen = 0;
    if (packet[0] == 0x02 && packet[1] == 0x59 && packet[2] == 0x4D && packet.length > 30) {
        minHeaderLen = 12;
    }

    for (var headerLen = minHeaderLen; headerLen < 20; ++headerLen) {
        var header = packet.slice(0, headerLen);
		var d = packet.slice(headerLen);
		var tlvArray = parseTLVArray(d, 0);
		if (!tlvArray) { continue; }
		return arrayToHex(header) + '\n'
			+ describeTLVArray(tlvArray, 1);
    }
    return arrayToHex(packet);
}

function describeTLVArray(tlvArray, indent) {
    var result = '';
	if (tlvArray.tlvs.length) {
		for(var i = 0; i < tlvArray.tlvs.length; ++i) {
			var tlv = tlvArray.tlvs[i];
			result += '  '.repeat(indent) + tlv.tag + ' ' + tlv.length;
			var children = parseTLVArray(tlv.value, 0);
			if (children && children.tlvs.length > 1) {
				result += '\n' + describeTLVArray(children, indent + 1);
			} else {
				result += ' ' + arrayToHex(tlv.value) + '\n';
			}
		}
	}
	if (tlvArray.rest) {
		result += '  '.repeat(indent) + tlvArray.rest + '\n';
    }
	return result;
}

function looksLikeHex(text) {
	return /^(([A-F0-9]){2})+$/i.test(text);
}

function looksLikeJavaStringOfHex(text) {
	return /^[A-Fa-f0-9\+\"\'\s\(\)]+;?$/.test(text);
}

function isPrintableChar(c) {
    return /[-._ \/\(\)\[\]a-zA-Z0-9]/.test(c);
}

function convertFromHexToAscii(hex) {
	var result = '';
	for (var i = 0; i < hex.length; i += 2) {
		var c = String.fromCharCode(parseInt(hex.substr(i, 2), 16));
		if (!isPrintableChar(c)) {
			return '';
		}
		result += c;
	}
	return result;
}

function formatDivAsHex(div) {
	var text = div.text();
	if (!looksLikeHex(text)) {
		return;
	}
	div.empty();
	div.append($("<span>").addClass("hex").text(text));

	var asAscii = convertFromHexToAscii(text);
	if (asAscii) {
		div.append($("<span>").addClass("hexdescr").text('("' + asAscii + '")'));
	}
}
