## TLV Formatter
* Formats TLV data to indented human-readable form
* Purely in client-side javascript (no secret data sent to some server)
* Can handle hex- or base64-encoded TLV input data
* Strips away usual java-like string symbols such as `"` and `+`, so pasting java code works.

[HTML version](https://oskarwiksten.gitlab.io/tlvformat/)
